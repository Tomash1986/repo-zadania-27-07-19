package com.sda.exceptionsexample;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;

public class CheckedRxceptions {
    public static void main(String[] args) {

        File file =new File("file.txt"); // tak robic nie tak
        FileReader fileReader; //tak robic to mozna pozniejs korzystac z redeara
        try {
            fileReader =new FileReader(file);
        } catch (FileNotFoundException e) {
            //kod ktory wykona sie po zlapaniu wyjatku
            e.printStackTrace();
        }

        // mozna co robic na tym ileReader
    }
}

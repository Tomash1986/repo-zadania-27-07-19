package com.sda.loops;


import com.sda.arrays.ArrayUtils;
import com.sda.string.ReversedWord;

import static com.sda.string.ReversedWord.reversedWord;

public class PrintExercise {

    public static void main(String[] args) {
        printTriangle(6);
        printDiagonal(6);
        printRectangle(6);
        System.out.println();
        printXXX(7);
        System.out.println();
        printStar(7);

        int [] arr =new int[8];

        arr[3]=100;
        arr[7]=1;
        ArrayUtils.swap(arr, 3,7);
        System.out.println(arr[3]);
        System.out.println(arr[7]);
        String word=reversedWord("Michal");
        System.out.println(word);
    }

    private static void printStar(int n) {
        for (int i = 0; i < n ; i++) {
            for (int j = 0; j < n ; j++) {
                if(i==j || j==n-i-1 ||j==n/2 || i==n/2)
                    System.out.print(" * ");
                else
                    System.out.print("   ");
            }
            System.out.println();
        }

    }

    private static void printXXX(int n) {

        for (int i = 0; i < n ; i++) {
            for (int j = 0; j < n ; j++) {
                if(i==j || j==n-i-1)
                System.out.print(" * ");
                else
                    System.out.print("   ");
            }
            System.out.println();
        }
    }

    private static void printRectangle(int n) {

        for (int i = 0; i <n ; i++) {

            if(i==0||i==n-1) {
                for (int j = 0; j<n; j++) {
                    System.out.print(" * ");
                }
                System.out.println();
            }else {
                for (int j = 0; j < n; j++) {
                    if (j == 0 || j == n - 1)
                        System.out.print(" * ");
                    else
                        System.out.print("   ");
                }
                System.out.println();
            }


        }

    }

    private static void printTriangle(int n) {

        for (int i = 0; i < n ; i++) {

            for (int j = 0; j <= i; j++) {
                System.out.print(" * ");
            }
            System.out.println();
        }
    }

    private static void printDiagonal(int n) {

        for (int i = 0; i < n ; i++) {

            for (int j = 0; j <= n+i; j++) {
                if (i!=j)
                    System.out.print("   ");
                else
                    System.out.print(" * ");
            }
            System.out.println();
        }
    }




}

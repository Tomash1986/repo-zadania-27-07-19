package com.sda.string;

public class ReversedWord {

    public static String reversedWord(String word){


        StringBuilder input1 = new StringBuilder(word);

        return input1.reverse().toString();
    }
}

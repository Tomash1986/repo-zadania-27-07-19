package com.sda.recursive;

public class reccur3 {

    public static double reccur3(int n){

        if (n==1)
            return 1;
        else if (n==2)
            return 0.5;
        else return -reccur3(n-1)*reccur3(n-2);
    }



}

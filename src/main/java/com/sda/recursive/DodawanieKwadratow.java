package com.sda.recursive;

public class DodawanieKwadratow {

    public static int addpow2(int n){
        if (n<0)
            return 0;
        else
            return (int)Math.pow(n,2) +addpow2(n-1);

    }
}

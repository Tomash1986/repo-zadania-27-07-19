package com.sda.interfaceexample;

import java.math.BigDecimal;

public interface Rentable {

    public String generateDescription();
    BigDecimal calculatePriceMin(int minutes);

}

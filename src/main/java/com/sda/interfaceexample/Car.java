package com.sda.interfaceexample;

import java.math.BigDecimal;

public class Car implements Rentable{

    private String brand;
    private String model;
    private int year;
    private BigDecimal pricePerMin;

    public Car() {
    }

    public Car(String brand, String model, int year, BigDecimal pricePerMin) {
        this.brand = brand;
        this.model = model;
        this.year = year;
        this.pricePerMin = pricePerMin;
    }

    public String getBrand() {
        return brand;
    }

    public String getModel() {
        return model;
    }

    public int getYear() {
        return year;
    }

    public BigDecimal getPricePerMin() {
        return pricePerMin;
    }


    public String generateDescription() {
        return "Car{" +
                "brand='" + brand + '\'' +
                ", model='" + model + '\'' +
                ", year=" + year +
                ", pricePerMin=" + pricePerMin +
                '}';
    }

    @Override
    public BigDecimal calculatePriceMin(int minutes) {
        return null;
    }


}

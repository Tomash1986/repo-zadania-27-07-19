package com.sda.interfaceexample;

import java.math.BigDecimal;

public class Main {
    public static void main(String[] args) {

        RentService rentService =new RentService();
        EmailService emailService =new EmailService();
        SmsService smsService =new SmsService();


        Scooter scooter =new Scooter("Lime", new BigDecimal("2.5"));
        Car car =new Car("Ford", "Astra",2005, new BigDecimal("10.5"));

   //     scooter.setBrand("Lime");
        //   scooter.setPricePerMin(new BigDecimal("2.5"));
        emailService.sendEmail(scooter);
        smsService.sendSMS(scooter);
        rentService.rent(scooter,10);

        emailService.sendEmail(car);
        smsService.sendSMS(car);
        rentService.rent(car,30);

//        System.out.println(scooter.getBrand());
//        System.out.println(scooter.getPricePerMin());


    }
}

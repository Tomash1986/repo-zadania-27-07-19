package com.sda.interfaceexample;

import java.math.BigDecimal;

public class Trailer implements Rentable{
    private BigDecimal pricePerMinute;

    public Trailer(BigDecimal price) {
        this.pricePerMinute = price;
    }

    public BigDecimal getPricePerMinute() {
        return pricePerMinute;
    }

    public String generateDescription() {
        return "Trailer{" +
                "price=" + pricePerMinute +
                '}';
    }

    @Override
    public BigDecimal calculatePriceMin(int minutes) {
        return null;
    }
}

package com.sda.interfaceexample;

import java.math.BigDecimal;

public class RentService {



    public void rent(Scooter scooter, int minutes){

        System.out.println("Renting..");
        BigDecimal totalPrice =scooter.getPricePerMin().multiply(new BigDecimal(minutes));
        System.out.println("Total price: "+ totalPrice);

    }

    public void rent(Car car, int minutes){

        System.out.println("Renting..");
        BigDecimal totalPrice =car.getPricePerMin().multiply(new BigDecimal(minutes));
        System.out.println("Total price: "+ totalPrice);

    }


    public void rent(Rentable rentable, int minutes){

        System.out.println("Renting..");
        BigDecimal totalPrice =rentable.calculatePriceMin(minutes);
        System.out.println("Total price: "+ totalPrice);

    }
}

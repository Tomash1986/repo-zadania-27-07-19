package com.sda.interfaceexample;

import java.math.BigDecimal;

public class Scooter implements Rentable {
    // double pricePerMin;
    // Double nie jest super dokłądny dlatego warto stosowac big ending trzeba stsowac BigDecimal
    // Nie robic seterow bo beda pieprzyc dane. Naprzykał pobieramy dane z bazy danych np id  i inne parametry i w projekci zminiamy ich wartosc
    // przez seter w programie go zmieniamy


    private String brand;
    private BigDecimal pricePerMin;

    public Scooter(){

    }

    public Scooter(String brand){
        this.brand= brand;
        this.pricePerMin=new BigDecimal(2);
    }

    public Scooter(String brand, BigDecimal pricePerMin){
        this.brand = brand;
        this.pricePerMin=pricePerMin;
    }

    public String getBrand(){
        return brand;
    }

    public BigDecimal getPricePerMin() {
        return pricePerMin;
    }

    public String generateDescription() {
        return "Scooter{" +
                "brand='" + brand + '\'' +
                ", pricePerMin=" + pricePerMin +
                '}';
    }

    @Override
    public BigDecimal calculatePriceMin(int minutes) {
        return pricePerMin;
    }
}

package com.sda.interfaceexample;

public class EmailService {

 //   public void sendEmail(Scooter scooter){
//        System.out.println("You have just rented a scooter "+ scooter.getBrand());
//        System.out.println("Price per minute: "+ scooter.getPricePerMin());
//    }
//
//    public void sendEmail(Car car){
//        System.out.println("You have just rented a car "+ car.getBrand());
//        System.out.println(car.generateDescription());
//    }
//
//    public void sendEmail(Trailer trailer){
//        System.out.println("You have just rented a trailer");
//        System.out.println(trailer.generateDescription());
//    }

    public void sendEmail(Rentable rentable){
        System.out.println("You have just rented");
        System.out.println(rentable.generateDescription());
    }
}

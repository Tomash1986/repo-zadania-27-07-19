package com.sda.interfaceexample;

public class SmsService {

//    public void sendSMS(Scooter scooter){
//        System.out.println("You have just rented a scooter "+ scooter.getBrand());
//        System.out.println("Price per minute: "+ scooter.getPricePerMin());
//    }
//
//    public void sendSMS(Car car){
//        System.out.println("You have just rented ");
//        System.out.println(car.generateDescription());
//    }


    public void sendSMS(Rentable rentable){
        System.out.println("You have just rented ");
        System.out.println(rentable.generateDescription());
    }
}

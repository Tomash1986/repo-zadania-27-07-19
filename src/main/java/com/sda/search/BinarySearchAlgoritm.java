package com.sda.search;

import java.util.Arrays;

public class BinarySearchAlgoritm implements SearchAlgorithm {

    public int search (int [] array, int value){

//        Arrays.sort(array);
//
//        return Arrays.binarySearch(array, value);
        int begin =0;
        int last=array.length-1;
        int mid=0;

        while(begin<=last){

            mid =(begin+last)/2;
            if(array[mid]<value){
                begin=mid+1;
            }
            else if (array[mid]>value){
                last=mid-1;
            }
            else{
                return mid;
            }
        }

        return -1;


    }
}

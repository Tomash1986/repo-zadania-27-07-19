package com.sda.search;

public class NaiveSearchAlgorithm  implements SearchAlgorithm {

    @Override
    public int search(int[] array, int value)   {

        if(array.length== 0)
            throw new NullPointerException();

        int index=0;
        boolean isOutOfScoop=false;
        for (int i = 0; i <array.length ; i++) {
            if(array[i]==value){
                index=i;
                isOutOfScoop=true;
            };
        }
        if(isOutOfScoop)
            return index;
        else
            return -1;
    }
}

package com.sda.structures.stack;

import java.util.EmptyStackException;

public class SdaStackImpl implements SdaStack {

    private int[] data;
    private int top;

    public SdaStackImpl(int capacity) {
        this.data = new int[capacity];
        this.top = -1;
    }

    @Override
    public void push(int value) {
        if (isFull()) {
            throw new StackOverflowError();
        }
        //data[top+1]=value;
        //top++;
        // lepiej
        data[++top] = value;

    }


    @Override
    public int pop() {
        if(isEmpty())
        {
            throw new EmptyStackException();
        }
        return data[top--];

    }


    @Override
    public int peek() {
        if (isEmpty()) {
            throw new EmptyStackException();
        }
        int value = data[top];
        return value;
    }


    @Override
    public int size() {
        return top + 1;
    }


    @Override
    public void clear() {
        //this.data = new int[data.length];
        this.top = -1;
        //     top=-1;

    }


    @Override
    public boolean isEmpty() {
        return top == -1;
    }


    @Override
    public boolean isFull() {
        return top == data.length - 1;
    }


}

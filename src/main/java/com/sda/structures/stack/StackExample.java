package com.sda.structures.stack;

import java.util.Stack;

public class StackExample {

    public static void main(String[] args) {
        Stack<String> stack= new Stack<>();

        stack.push("A");
        stack.push("B");
        stack.push("C");
        stack.push("D");

        System.out.println("Top is : " + stack.peek());

        System.out.println("Stack size is :" + stack.size());

        stack.pop();
        stack.pop();
        stack.pop();

        System.out.println("Stack size is :" + stack.size());

        if ((stack.empty())){
            System.out.println("Stackt is empty ");
        }else
            System.out.println("Stack is not empty");
    }
}

package com.sda.structures.stack;

public interface SdaStack {

    void push (int value);

    int pop();

    int peek();

    int size();

    void clear();

    boolean isEmpty();

    boolean isFull();
}

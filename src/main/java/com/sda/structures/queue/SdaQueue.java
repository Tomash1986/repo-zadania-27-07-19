package com.sda.structures.queue;

public interface SdaQueue {

    void enqueue (int value);

    int dequeue ();

    int size();

    void clear();

    int peek();

    boolean isEmpty();

    boolean isFull();
}

package com.sda.structures.queue;

import java.util.NoSuchElementException;

public class SdaQueueImpl implements SdaQueue {

    private int[] data;
    private int first;
    private int last;
    private int size;

    public SdaQueueImpl(int capacity) {
        this.data = new int [capacity];
        this.first = 0;
        this.last = -1;
        this.size=0;
    }

    @Override
    public void enqueue(int value) {
        if(isFull()){
            throw new IndexOutOfBoundsException();
        }

        data[++last]=value;
        size++;

    }

    @Override
    public int dequeue() {
        if(isEmpty()){
            throw new NoSuchElementException();
        }

   // To trzeba sprawdzic to co on zakomituje

        size--;
        int value = data[first++];
        int tmp=0;
        for (int i = first; i < data.length-1 ; i++) {
            data[i-1]=data[i];
        }
        first--;
        last--;
        return value;

    }

    @Override
    public int size() {
        return size;
    }

    @Override
    public void clear() {
        first = 0;
        last = -1;
        size=0;
    }

    @Override
    public int peek() {

        return data[first];
    }

    @Override
    public boolean isEmpty() {
        return size==0;
    }

    @Override
    public boolean isFull() {
        return size ==data.length;
    }
}

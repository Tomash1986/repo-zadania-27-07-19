package com.sda.structures.list;

public interface SdaGenericList <T>  {

    void add(T value);

    T get(int index);

    boolean remove(int index);

    void clear();

    int size();

    boolean contains(T value);

    boolean isEmpty();

    void printList();
}

package com.sda.structures.list;

public class SdaListImpl implements SdaList {

    private  Node head;


    @Override
    public void add(int value) {

        Node node =new Node(value);


        if (head==null){
            head = node;
        }
        else
        {
            // zapisuje pierszy elemnt do tmp
            Node tmp=head;

            while (tmp.next!=null){
                // wiemy ze tmp ma nastepny element
                tmp =tmp.next;
            }
            // tutja jest ostani elemnt
            tmp.next=node;


        }

    }


    @Override
    public int get(int index) {

        int count =0;
        Node tmp =head;

        while(tmp!=null){
            if(count == index){
                return tmp.value;
            }
            else{
                tmp=tmp.next;
                count++;
            }

        }
        throw new IndexOutOfBoundsException();

    }

    @Override
    public boolean remove(int index) {
        int count =0;
        Node tmp =head;
        boolean isRemoved =false;
        if(index==0)
        {
            head=head.next;
            isRemoved = true;
        }
        else {
            while (tmp != null) {

                if (index == count) {
                    tmp.next = tmp.next.next;
                    isRemoved = true;
                } else {
                    tmp = tmp.next;
                    count++;
                }
            }
        }

        return isRemoved;

    }

    @Override
    public void clear() {

        head =null;


    }
    @Override
    public int size() {

        Node temp = head;
        int count = 0;
        while (temp != null)
        {
            count++;
            temp = temp.next;
        }
        return count;
    }

    @Override
    public boolean contains(int value) {
        return false;
    }

    @Override
    public boolean isEmpty() {
        return head == null;
    }

    @Override
    public void printList() {
        Node tmp= head;
        while (tmp!=null){
            System.out.println(tmp.value);
            tmp =tmp.next;
        }
    }


    private class  Node{

        private  int value;
        private  Node next;

        private Node(int value){
            this.value=value;
        }


    }



}

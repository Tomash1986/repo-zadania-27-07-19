package com.sda.sort.objects;

import java.util.Comparator;

public class ForumUser implements  Comparable<ForumUser>{

    private String name;
    private String surname;
    private int postNumber;

    public ForumUser(String name, String surname, int postNumber) {
        this.name = name;
        this.surname = surname;
        this.postNumber = postNumber;
    }


    public String getName() {
        return name;
    }

    public String getSurname() {
        return surname;
    }

    public int getPostNumber() {
        return postNumber;
    }

    @Override
    public String toString() {
        return "ForumUser{" +
                "name='" + name + '\'' +
                ", surname='" + surname + '\'' +
                ", postNumber=" + postNumber +
                '}';
    }

    @Override
    public int compareTo(ForumUser o) {
        return this.name.compareTo(o.getName());
    }

    public static Comparator<ForumUser> getPostsComparator(){
        return new Comparator<ForumUser >() {
            @Override
            public int compare(ForumUser o1, ForumUser o2) {
//                if(o1.getPostNumber()>o2.getPostNumber())
//                    return 1;
//                else if (o1.getPostNumber()==o2.getPostNumber())
//                    return 0;
//                else
//                    return -1;
               // return Integer.valueOf(o1.getPostNumber()).compareTo(o2.getPostNumber());
                // Aby moznabylo porownywac  przy wykorzystaniu comper trzeba zrzutowc na Intiger
                return Integer.compare(o1.getPostNumber(),o2.getPostNumber());

            }
        };

    }

}

package com.sda.sort;

public class BubbleSort implements SortAlgorithm {


    public void sort(int[] arr) {
        boolean is_sorted = true;
        for (int i = 0; i <arr.length ; i++) {
                for (int j = 0; j < arr.length-i-1 ; j++) {
                    if(arr[j]>arr[j+1]) {
                        swap(arr, j, j + 1);
                        is_sorted=false;
                    }
                }
        if (is_sorted)
            return ;
        }

    }



    private static void swap(int[] array, int idx1, int idx2){
        int temp;
        temp=array[idx1];
        array[idx1]=array[idx2];
        array[idx2]=temp;

    }
}

package com.sda.generics;


import com.sda.structures.list.SdaGenericList;
import com.sda.structures.list.SdaGenericListImpl;

public class GenericExample {

    public static void main(String[] args) {
        Box<String> box =new Box<>("Wartosc");

        System.out.println(box.getValue());

        Box<Integer> box2 =new Box<>(15);

        System.out.println(box2.getValue());

        SdaGenericList<String> list = new SdaGenericListImpl<>();

        list.add("test");
        list.add("test");
        list.add("test");
        list.add("test");

        System.out.println(list.size());
    }

}

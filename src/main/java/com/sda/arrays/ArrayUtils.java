package com.sda.arrays;

import java.util.Arrays;

public class ArrayUtils {

    public static void swap(int[] array, int idx1, int idx2){
        int temp;
        temp=array[idx1];
        array[idx1]=array[idx2];
        array[idx2]=temp;

    }

    public static int findSecondMax(int [] arr){

        int max=Integer.MIN_VALUE;
        int jmax=0;
        int max2=Integer.MIN_VALUE;
        for (int i = 0; i <arr.length ; i++) {

            if(arr[i]>max)
            {
                jmax=i;
            }

        }

        for (int i = 0; i <arr.length ; i++) {

            if(arr[i]>max2 && i!= jmax){
                max2=arr[i];
            }

        }
        return max2;
    }


    // Jezeli chodzi o kwoty to nie uzywamy double tylko bigdecimal!!!!!


    // Przeanalizuj
    public static int findSecondMaxOtimazed(int [] arr){

        int max=Integer.MIN_VALUE;
        int max2=Integer.MIN_VALUE;

        for (int i = 1; i <arr.length ; i++) {

            if(arr[i]>max) {
                max2 = max;
                max = arr[i];
            }else if(arr[i]>max2){
                max2=arr[i];
            }


        }


        return max2;
    }

}

package com.sda.sort.objects;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.lang.reflect.Array;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.stream.Collectors;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class ForumUserTest {

    private List<ForumUser> users;

    @BeforeEach
    public void setUP() {
        System.out.println("Set up");
        users = new ArrayList<>();
        users.add(new ForumUser("Jan", "Nowak", 5023));
        users.add(new ForumUser("Andrzej", "Zielinski", 505));
        users.add(new ForumUser("Wieslaw", "Kowalski", 5425));
        users.add(new ForumUser("Michal", "Polak", 25));
        users.add(new ForumUser("Kazimierz", "Wielki", 1234));
        users.add(new ForumUser("Andrzej", "Nowak", 505));

    }

    @Test
    public void shouldSortByFisrtName() {
        // given
        //    List<ForumUser> users =new ArrayList<>(); // Jak metoda przyjmuje liste to zawsze tworzymy List !!!!!!! nie ArrayList

        //   ForumUser fu = new ForumUser("Andrzej", "Zielinski", 505);
        //  ForumUser fu2 = new ForumUser("Jan", "Nowak", 502);
        //   users.add(fu);
        //  users.add(fu2);


        //        ForumUser [] array= {
//                new ForumUser("Andrzej", "Zielinski", 505),
//                new ForumUser("Andrzej", "Zielinski", 505),
//                new ForumUser("Andrzej", "Zielinski", 505),
//        };


        // when
        Collections.sort(users);

        //then
        assertThat(users.get(0).getName()).isEqualTo("Andrzej");


        System.out.println(users);

    }

    @Test
    public void shouldSortByFisrtNameUsingStream() {

        List<ForumUser> sortedList = users.stream().sorted().collect(Collectors.toList()); //Strumien działa na kopi danych i zwraca
        //nowa liste

        // when
        Collections.sort(users);

        //then
        assertThat(users.get(0).getName()).isEqualTo("Andrzej");


        System.out.println(users);

    }

    @Test
    public void shouldSortByFisrtNameandLastName() {
        // given


        // when
        Collections.sort(users, new Comparator<ForumUser>() {
            @Override
            public int compare(ForumUser o1, ForumUser o2) {
                int firstNameResult=o1.getName().compareTo(o2.getName());

                if (firstNameResult==0){
                    // sprawdzic po nazwiskach
                    return o1.getSurname().compareTo(o2.getSurname());

                }
                return firstNameResult;
            }
        });

        //then
        assertThat(users.get(0).getName()).isEqualTo("Andrzej");
        assertThat(users.get(0).getSurname()).isEqualTo("Nowak");

        System.out.println(users);

    }

    @Test
    public void shouldSortByPostsUsingStaticComparatorFromClass(){

        Collections.sort(users,ForumUser.getPostsComparator());

        assertThat(users.get(0).getPostNumber()).isEqualTo(25);
        assertThat(users.get(5).getPostNumber()).isEqualTo(5425);

    }

}
package com.sda.search;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class NaiveSearchAlgorithmTest {


    private SearchAlgorithm searchAlgorithm = new NaiveSearchAlgorithm();


    @Test
    public void shouldFindelemntInArray() {

        // given
        int array[] = {15, 7, 2, 6, 1, 13, 8};
        //when
        int resultidx = searchAlgorithm.search(array, 13);

        //then
        assertThat(resultidx).isEqualTo(5);


    }

    @Test
    public void shouldFindElemntInSortedArrey() {

        // given

        int array[] = {1, 2, 6, 7, 8, 13, 15};
        //when
        int resultidx = searchAlgorithm.search(array, 8);

        //then
        assertThat(resultidx).isEqualTo(4);


    }

    @Test
    public void shouldFindInvalidIndexIfElementNotPresent(){


        int array[] = {1, 2, 6, 7, 8, 13, 15};
        //when
        int resultidx = searchAlgorithm.search(array, 30);

        //then
        assertThat(resultidx).isEqualTo(-1);

    }

    @Test
    public void shouldFindInvalidIndexIfArrayLenghtIsZero(){


        int[] array = {};
        //when
        int resultidx = searchAlgorithm.search(array, 30);

        //then
        assertThat(resultidx).isEqualTo(-1);

    }

    @Test
    public void shouldThrowExceptionIfArrayIsNull(){


        int[] array = null;
        //when

        //then
        assertThrows(NullPointerException.class, () -> {
            searchAlgorithm.search(array, 21);
        });

    }


}
package com.sda.arrays;

import com.sda.arrays.ArrayUtils;
import org.junit.jupiter.api.Test;

import static com.sda.arrays.ArrayUtils.swap;
import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

public class ArrayTest {

    @Test
    public void shouldSwapTwoElemnets(){

        //given
        int [] array ={3,7,1,5,8,12,23};

        //when
        ArrayUtils.swap(array,3,5);
        //then
        assertThat(array[3]).isEqualTo(12);
        assertThat(array[5]).isEqualTo(5);

    }
@Test
    public void shouldSecendBiggestValue(){

        //given
        int [] array ={3,7,1,5,8,12,23};

        //when
        int value=ArrayUtils.findSecondMax(array);
        //then
        assertThat(value).isEqualTo(12);

    }

}

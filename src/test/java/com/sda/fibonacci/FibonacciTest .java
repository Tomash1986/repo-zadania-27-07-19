package com.sda.fibonacci;

import org.junit.jupiter.api.Disabled;
import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class FibonacciTest {

    private Fibonacci fibonacci = new Fibonacci();

    @Test
    public void shouldReturnCorrectValueI() {
        //given
        int n = 18;

        //when
        int result = fibonacci.fib(n);

        //then
        assertThat(result).isEqualTo(2584);
    }


    @Test
    public void shouldReturnZeroFibElementI() {
        //given
        int n = 0;

        //when
        int result = fibonacci.fib(n);

        //then
        assertThat(result).isEqualTo(0);
    }


    @Test
    public void shouldReturnFirstFibElementI() {
        //given
        int n = 1;

        //when
        int result = fibonacci.fib(n);

        //then
        assertThat(result).isEqualTo(1);
    }


    @Test
    public void shouldReturnSecondFibElementI() {
        //given
        int n = 2;

        //when
        int result = fibonacci.fib(n);

        //then
        assertThat(result).isEqualTo(1);
    }


    @Test
    public void shouldNotCalculateNegativeElementI() {
        //given
        int n = -5;

        //when & then
        assertThrows(IllegalArgumentException.class, () -> {
            fibonacci.fib(n);
        });
    }

}

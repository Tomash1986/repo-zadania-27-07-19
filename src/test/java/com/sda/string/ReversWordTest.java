package com.sda.string;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;


public class ReversWordTest {
    @Test
    public void test(){

        // given
        String word="Michal";

        // when
        String reversedWord=ReversedWord.reversedWord(word);
        // then
        assertThat(reversedWord).isEqualTo("lahciM");
    }

}

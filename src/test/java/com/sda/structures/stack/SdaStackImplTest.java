package com.sda.structures.stack;

import org.junit.jupiter.api.Test;

import java.util.EmptyStackException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.assertThrows;

class SdaStackImplTest {

    @Test
    void shouldPushElement() {
        //given
        SdaStack stack =new SdaStackImpl(5);

        //when
        stack.push(5);
        stack.push(2);

        //then
        assertThat(stack.size()).isEqualTo(2);
    }


    @Test
    void shouldThrowStackOverflowError(){
        //given
        SdaStack stack =new SdaStackImpl(2);
        stack.push(5);
        stack.push(4);

        // when then

        assertThrows(StackOverflowError.class, () -> {
            stack.push(10);
        });;

    }

    @Test
    void shouldPopElementFromStack() {
        //given
        SdaStack stack =new SdaStackImpl(10);
        stack.push(5);
        stack.push(4);
        stack.push(3);
        stack.push(1);
        stack.push(7);

        //then
        int value=stack.pop();

        //then
        assertThat(value).isEqualTo(7);
        assertThat(stack.size()).isEqualTo(4);

    }

    @Test
    void shouldNotPopElementFromEmptyStack() {
        //given
        SdaStack stack = new SdaStackImpl(10);

        //when & then
        assertThrows(EmptyStackException.class, stack::pop);
    }



    @Test
    void shouldPeekLastElement() {
        //given
        SdaStack stack = new SdaStackImpl(50);
        stack.push(5);
        stack.push(2);
        stack.push(4);
        stack.push(2);

        //when & then
        assertThat(stack.peek()).isEqualTo(2);
        assertThat(stack.size()).isEqualTo(4);
    }

    void shouldPeekEmptyStack() {
        //given
        SdaStack stack = new SdaStackImpl(5);

        //when & then
        assertThrows(EmptyStackException.class, stack::peek);
    }


    @Test
    void size() {
        //given
        SdaStack stack = new SdaStackImpl(3);
        stack.push(5);
        stack.push(2);
        stack.push(15);

        //when & then
        assertThat(stack.size()).isEqualTo(3);

    }

    @Test
    void shouldClearStack() {

        //given
        SdaStack stack = new SdaStackImpl(3);
        stack.push(2);
        stack.push(4);

        //when
        stack.clear();
        stack.push(5);

        //then
        assertThat(stack.size()).isEqualTo(1);
    }





    @Test
    void shouldReturnTrueIfStackIsEmpty() {
        //given
        SdaStack stack = new SdaStackImpl(2);

        //when & then
        assertThat(stack.isEmpty()).isTrue();
    }

    @Test
    void shouldReturnFalseIfStackIsNotEmpty() {
        //given
        SdaStack stack = new SdaStackImpl(3);
        stack.push(2);
        stack.push(4);
        stack.push(6);


        //when & then
        assertThat(stack.isEmpty()).isFalse();
    }




    @Test
    void shouldReturnTrueIfStackIsFull() {
        //given
        SdaStack stack = new SdaStackImpl(2);
        stack.push(2);
        stack.push(4);

        //when/then
        assertThat(stack.isFull()).isTrue();

    }

    @Test
    void shouldReturnFalseIfStackIsFull() {
        //given
        SdaStack stack = new SdaStackImpl(200);
        stack.push(2);
        stack.push(4);

        //when/then
        assertThat(stack.isFull()).isFalse();

    }

}
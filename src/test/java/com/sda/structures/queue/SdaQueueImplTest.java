package com.sda.structures.queue;

import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;

import java.util.NoSuchElementException;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class SdaQueueImplTest {

    private SdaQueue sdaQueue;

    @BeforeEach
    public void setUp() {
        sdaQueue = new SdaQueueImpl(10);
    }

    @Test
    void shouldEnqueueElement() {
        //when
        sdaQueue.enqueue(2);
        sdaQueue.enqueue(5);

        //then
        assertThat(sdaQueue.peek()).isEqualTo(2);
        assertThat(sdaQueue.size()).isEqualTo(2);
    }

    @Test
    void shouldThrowExepcionWhenEnqueueToFullQueue() {
        //given
        SdaQueue queue = new SdaQueueImpl(1);
        queue.enqueue(2);

        //when & then
        assertThrows(IndexOutOfBoundsException.class, () -> queue.enqueue(5));
    }

    @Test
    void shouldDequeueElement() {
        //given
        sdaQueue.enqueue(5);
        sdaQueue.enqueue(3);
        sdaQueue.enqueue(7);
        sdaQueue.enqueue(2);
        sdaQueue.enqueue(6);
        sdaQueue.enqueue(8);
        //when
        sdaQueue.dequeue();
        sdaQueue.dequeue();
        sdaQueue.dequeue();

        //then
        assertThat(sdaQueue.size()).isEqualTo(3);
        assertThat(sdaQueue.peek()).isEqualTo(2);
    }

    @Test
    void shouldNotDequeueElementFromEmptyQueue() {
        //when & then
        assertThrows(NoSuchElementException.class, () -> {
            sdaQueue.dequeue();
        });
    }

    @Test
    void shouldReturnActualSize() {
        //given
        sdaQueue.enqueue(5);
        sdaQueue.enqueue(4);

        //when & then
        assertThat(sdaQueue.size()).isEqualTo(2);
    }

    @Test
    void clear() {
        //given
        sdaQueue.enqueue(17);
        sdaQueue.enqueue(17);

        //when
        sdaQueue.clear();

        //then
        assertThat(sdaQueue.size()).isEqualTo(0);
    }

    @Test
    void shouldPeek() {
        //given
        sdaQueue.enqueue(9);
        sdaQueue.enqueue(8);
        sdaQueue.enqueue(7);
        sdaQueue.enqueue(3);

        //when & then
        assertThat(sdaQueue.peek()).isEqualTo(9);
        assertThat(sdaQueue.size()).isEqualTo(4);
    }

    @Test
    void shouldReturnTrueIfQueueIsEmpty() {
        //when & then
        assertThat(sdaQueue.isEmpty()).isTrue();
    }


    @Test
    void shouldReturnFalseIfQueueIsNotEmpty() {
        //when
        sdaQueue.enqueue(9);

        //when & then
        assertThat(sdaQueue.isEmpty()).isFalse();
    }

    @Test
    void shouldReturnTrueIfQueueIstFull() {
        //given
        SdaQueue queue = new SdaQueueImpl(2);
        queue.enqueue(2);
        queue.enqueue(5);

        //when & then
        assertThat(queue.isFull()).isTrue();
    }

    @Test
    void shouldReturnFalseIfQueueIsNotFull() {
        //given
        sdaQueue = new SdaQueueImpl(3);
        sdaQueue.enqueue(3);
        sdaQueue.enqueue(5);

        //when & then
        assertThat(sdaQueue.isFull()).isFalse();
    }

    @Test
    void shouldWork() {
        //given
        SdaQueue queue = new SdaQueueImpl(4);
        queue.enqueue(2);
        queue.enqueue(5);
        queue.enqueue(5);

        queue.dequeue();
        queue.dequeue();
        queue.dequeue();

        queue.enqueue(2);
        queue.enqueue(7);
        queue.enqueue(9);

        //when & then
        assertThat(queue.size()).isEqualTo(3);
    }

}
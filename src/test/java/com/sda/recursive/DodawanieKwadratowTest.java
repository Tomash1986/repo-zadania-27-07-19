package com.sda.recursive;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class DodawanieKwadratowTest {

    @Test
    public void shouldCalculateFactorial() {
        //given & when
        long result = DodawanieKwadratow.addpow2(2);

        //then
        assertThat(result).isEqualTo(5);
    }
}
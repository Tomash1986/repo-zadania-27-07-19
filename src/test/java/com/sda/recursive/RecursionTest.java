package com.sda.recursive;

import org.assertj.core.internal.bytebuddy.build.ToStringPlugin;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.TestTemplate;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class RecursionTest {

    @Test
    public void shouldReturnCorrectValueI4() {
        //given


        //when
        int result = Recursion.factorial(4);

        //then
        assertThat(result).isEqualTo(24);
    }

    @Test
    public void shouldReturnCorrectValueI10() {
        //given


        //when
        int result = Recursion.factorial(10);

        //then
        assertThat(result).isEqualTo(3628800);
    }

}
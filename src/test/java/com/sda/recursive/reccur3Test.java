package com.sda.recursive;

import org.junit.jupiter.api.Test;

import static org.assertj.core.api.Assertions.assertThat;
import static org.junit.jupiter.api.Assertions.*;

class reccur3Test {

    @Test
    public void shouldReturnCorrectValueI() {
        //given
        int n = 3;

         // 1; 0.5 ; -0,5 ; 0.25 ; -0.125;
        //when
        double result = reccur3.reccur3(3);

        //then
        assertThat(result).isEqualTo(-0.5);
    }
}